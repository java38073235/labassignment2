package application;

import vehicles.Bicycle;

public class BikeStore 
{
 public static void main(String[]args)
 {
    Bicycle bikes[] = new Bicycle[4];
    bikes[0]=new Bicycle("old",3,5.05);
    bikes[1]=new Bicycle("new",5,105.05);
    bikes[2]=new Bicycle("rusty",4,15.05);
    bikes[3]=new Bicycle("dusty",3,25.05);
   
    for(Bicycle x:bikes)
   {
      System.out.println(x);
   }
}
    
   

}
