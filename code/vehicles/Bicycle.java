package vehicles;


public class Bicycle
{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    
    public static String getManufacturer(String manufacturer)
    {
        return manufacturer;
    }

    public static int getnumberGears(int numberGears)
    {
        return numberGears;
    }
    public static double getMaxSpeed(double maxSpeed)
    {
        return maxSpeed;
    }

    public Bicycle( String manufacturerInput, int numberGearsInput, double maxSpeedInput)
    {
        manufacturer=manufacturerInput;
        numberGears=numberGearsInput;
        maxSpeed=maxSpeedInput;
    }

    public String toString()
    {
        return "Manufacturer: "+manufacturer+" Number of Gears: " + numberGears + ", MaxSpeed: " + maxSpeed;
    }
}